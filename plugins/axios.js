import Vue from 'vue';

export default function ({ app, $axios, redirect }) {
    $axios.onRequest(config => {
        // Get token
        let token = app.$auth.$storage.getLocalStorage('_token.local')
        // if logged in set header request
        if (app.$auth.loggedIn) {
            $axios.setHeader('Authorization', token)
        }

        console.log('Making request to ' + config.url)
    })

    $axios.onError(error => {
        let message = error.response.data;
        
        if (typeof message.message != undefined && message.code != 422 && message.code != 400) {
            Vue.swal({
                type: 'error',
                text: message.message
            });
        }

        if (typeof message.message != undefined && message.code == 422) {
            for (let name in message.message_array) {
                // Get input element by name
                let elem = document.getElementsByName(name)[0]
                // Create new <small> element
                let newNode = document.createElement('small')
                // Create text
                let text = document.createTextNode(message.message_array[name])

                // Add class to <small>
                newNode.classList.add("input-error-text")
                // Add text to <small>
                newNode.appendChild(text)
                // Insert after input element
                elem.after(newNode)
                // Add class error to input element
                elem.classList.add("input-error")
            }
        }

        return true
    })
}