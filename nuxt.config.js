const pkg = require('./package')

require('dotenv').config()

module.exports = {
    mode: 'spa',

    /*
    ** Headers of the page
    */
    head: {
        title: process.env.APP_NAME,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css' },
            { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.8.2/css/all.css', integrity: 'sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay', crossorigin: 'anonymous' }
        ],
        script: [
            { type: "text/javascript", src: "https://code.jquery.com/jquery-3.3.1.slim.min.js", integrity: 'sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo', crossorigin: 'anonymous' },
            { type: "text/javascript", src: "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js", integrity: 'sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49', crossorigin: 'anonymous' },
            { type: "text/javascript", src: "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js", integrity: 'sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy', crossorigin: 'anonymous' },
        ],
    },

    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#fff' },

    /*
    ** Global CSS
    */
    css: [
        '~/assets/css/main.css',
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        { src: '~/plugins/axios.js' },
        { src: '~/plugins/sweetalert.js' },
        { src: '~/plugins/spinner.js' },
        { src: '~/plugins/directives.js' },
        { src: '~/models/model.js' },
        { src: '~/models/endpoint.js' },
        { src: '~/helpers/helper.js' }
    ],

    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://axios.nuxtjs.org/usage
        '@nuxtjs/axios',
        '@nuxtjs/auth',
        '@nuxtjs/dotenv',
        'vue-sweetalert2/nuxt'
    ],

    /*
    ** Axios module configuration
    */
    axios: {
    // See https://github.com/nuxt-community/axios-module#options
    },

    /**
     * Router configuration
     */
    router: {
        middleware: ['auth']
    },

    /**
     * Auth Module
     */
    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: { url: 'user/login', method: 'post', propertyName: 'data.token' },
                    logout: false,
                    user: { url: 'userdetail/admin', method: 'get', propertyName: 'data' }
                },
                tokenRequired: true,
                tokenType: ''
            },
            redirect: {
                login: '/'
            }
        }
    },

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
            // Run ESLint on save
            if (ctx.isDev && ctx.isClient) {
                // config.module.rules.push({
                //     enforce: 'pre',
                //     test: /\.(js|vue)$/,
                //     loader: 'eslint-loader',
                //     exclude: /(node_modules)/
                // })
            }
        },
        extractCSS: true,
        loaders: [
            {
                test: /\.(png|jpe?g|gif|svg|webp)$/,
                loader: 'file-loader',
                query: {
                  limit: 1000, // 1kB
                  name: 'img/[name].[hash:7].[ext]'
                }
              }
        ]
    },
    server: {
        port: process.env.PORT, // default: 3000
        host: process.env.HOST // default: localhost
    }
}
