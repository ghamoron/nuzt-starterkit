const endpoint = () => ({
    login: 'user/login',
    dashboard: 'cms/dashboard',
    sales: {
        product: 'cms/sales/byProduct'
    }
})

export default ({
    app
}, inject) => {
    inject('endpoint', endpoint())
}