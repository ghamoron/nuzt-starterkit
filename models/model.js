const model = $axios => ({
    get(endpoint, params, showDataOnly = true) {
        let getData = $axios.get(endpoint, {
            params: params
        })

        if (showDataOnly) {
            return getData.then((res) => {

                if (res.data) {
                    return res.data.data
                }
    
                return false
            });
        }

        return getData
    },
    post(endpoint, params) {
        return $axios.post(endpoint, params)
        .then((res) => {
            if (res.data) {
                return res.data
            }

            return false
        })
    }
})

export default ({
    $axios
}, inject) => {
    inject('model', model($axios))
}