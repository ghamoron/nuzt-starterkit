const helper = () => ({
    clearInput() {
        let elems = document.querySelectorAll('.input-error')
        let elemsText = document.querySelectorAll('.input-error-text')

        for (var i = 0; i < elems.length; i++) {
            elems[i].classList.remove("input-error")
            elemsText[i].parentNode.removeChild(elemsText[i])
        }
    },
    formatMoney(num, currency = 'Rp ') {
        let format = num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')

        return `${currency} ${format}`
    }
})

export default ({
    app
}, inject) => {
    inject('helper', helper())
}